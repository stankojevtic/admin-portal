﻿using System;
using System.Collections.Generic;
using System.Linq;
using AdministrationPortal.Data.Context;
using AdministrationPortal.Interfaces.Generic;
using Microsoft.EntityFrameworkCore;

namespace AdministrationPortal.Repositories.Repositories
{
    public class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        private readonly DatabaseContext _dbContext;

        public BaseRepository(DatabaseContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IEnumerable<T> GetAll()
        {
            return _dbContext.Set<T>();
        }

        public void Add(T entity, bool saveChanges = true)
        {
            _dbContext.Set<T>().Add(entity);
            if (saveChanges) _dbContext.SaveChanges();
        }

        public void BulkInsert(IEnumerable<T> entities, bool saveChanges = true)
        {
            foreach (T entity in entities)
            {
                _dbContext.Set<T>().Add(entity);
            }

            if (saveChanges) _dbContext.SaveChanges();
        }

        public void BulkUpdate(IEnumerable<T> entities, bool saveChanges = true)
        {
            foreach (T entity in entities)
            {
                _dbContext.Entry(entity).State = EntityState.Modified;
            }

            if (saveChanges) _dbContext.SaveChanges();
        }

        public void Delete(T entity, bool saveChanges = true)
        {
            if (entity == null)
                throw new NullReferenceException();

            _dbContext.Set<T>().Remove(entity);
            if (saveChanges) _dbContext.SaveChanges();
        }

        public void Edit(T entity, bool saveChanges = true)
        {
            _dbContext.Entry(entity).State = EntityState.Modified;
            if (saveChanges) _dbContext.SaveChanges();
        }

        public T Find(int id)
        {
            return _dbContext.Set<T>().Find(id);
        }


        public void Save()
        {
            _dbContext.SaveChanges();
        }

        public IQueryable<T> GetQueryable()
        {
            return _dbContext.Set<T>();
        }
    }
}
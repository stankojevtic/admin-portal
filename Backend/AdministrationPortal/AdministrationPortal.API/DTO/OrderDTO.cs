﻿using System;
using System.Collections.Generic;

namespace AdministrationPortal.API.DTO
{
    public class OrderDTO
    {
        public int OrderID { get; set; }
        public string TotalPrice { get; set; }
        public string CreatedDate { get; set; }        
        public List<OrderItemDTO> OrderItems { get; set; }
    }

    //move to another file
    public class OrderItemDTO
    {
        public int OrderItemID { get; set; }
        public int ProductID { get; set; }
        public string ProductCaption { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
    }
}

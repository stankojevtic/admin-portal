﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AdministrationPortal.API.DTO
{
    public class ProductCreateDTO
    {
        [Required]
        public string Caption { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public double Price { get; set; }
        [Required]
        public int BrandType { get; set; }
        [Required]
        public List<object> Categories { get; set; }
        [Required]
        public List<object> SizeTypes { get; set; }
    }
}

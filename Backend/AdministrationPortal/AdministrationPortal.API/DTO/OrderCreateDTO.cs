﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AdministrationPortal.API.DTO
{
    public class OrderCreateDTO
    {
        [Required]
        public int TotalPrice { get; set; }
        [Required]
        public List<object> OrderProducts { get; set; }
    }
}

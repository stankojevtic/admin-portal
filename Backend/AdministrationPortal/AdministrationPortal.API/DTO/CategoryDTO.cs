﻿namespace AdministrationPortal.API.DTO
{
    public class CategoryDTO
    {
        public int CategoryID { get; set; }
        public string Caption { get; set; }
    }
}

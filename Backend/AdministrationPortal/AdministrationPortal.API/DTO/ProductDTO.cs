﻿using System.Collections.Generic;

namespace AdministrationPortal.API.DTO
{
    public class ProductDTO
    {
        public int ProductID { get; set; }
        public string Caption { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public string BrandType { get; set; }
        public List<string> Categories { get; set; }
        public List<string> SizeTypes { get; set; }
    }
}

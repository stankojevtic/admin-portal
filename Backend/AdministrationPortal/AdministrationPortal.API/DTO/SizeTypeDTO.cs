﻿namespace AdministrationPortal.API.DTO
{
    public class SizeTypeDTO
    {
        public int SizeTypeID { get; set; }
        public string Caption { get; set; }
    }
}

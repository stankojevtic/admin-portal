﻿namespace AdministrationPortal.API.DTO
{
    public class BrandTypeDTO
    {
        public int BrandTypeID { get; set; }
        public string Caption { get; set; }
    }
}

﻿using System;
using System.Linq;
using AdministrationPortal.API.DTO;
using AdministrationPortal.Domain.Entities;
using AutoMapper;

namespace AdministrationPortal.API.AutoMapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Product, ProductDTO>()
                .ForMember(x => x.BrandType, y => y.MapFrom(e => e.BrandType.Caption))
                .ForMember(x => x.Categories, y => y.MapFrom(e => e.ProductCategories.Select(x => x.Category.Caption).ToList()))
                .ForMember(x => x.SizeTypes, y => y.MapFrom(e => e.ProductSizeTypes.Select(x => x.SizeType.Caption).ToList()))
                .ReverseMap();

            CreateMap<Product, ProductCreateDTO>()                
                .ReverseMap()
                .ForMember(x => x.BrandTypeID, y => y.MapFrom(e => e.BrandType))
                .ForMember(x => x.Uid, y => y.MapFrom(e => Guid.NewGuid()))
                .ForMember(x => x.BrandType, y => y.Ignore());

            CreateMap<Product, ProductUpdateDTO>()                
                .ReverseMap()
                .ForMember(x => x.BrandTypeID, y => y.MapFrom(e => e.BrandType))
                .ForMember(x => x.BrandType, y => y.Ignore());

            CreateMap<Order, OrderDTO>()
                .ForMember(x => x.CreatedDate, y => y.MapFrom(e => e.CreateDateUtc.ToString("yyyy-MM-dd HH':'mm':'ss")))                    
                .ReverseMap();

            CreateMap<OrderItem, OrderItemDTO>()
                .ForMember(x => x.ProductCaption, y => y.MapFrom(e => e.Product.Caption))
                .ForMember(x => x.Price, y => y.MapFrom(e => e.Product.Price))
                .ForMember(x => x.ProductID, y => y.MapFrom(e => e.Product.ProductID))                
                .ReverseMap();

            CreateMap<Order, OrderCreateDTO>()
                .ReverseMap()
                .ForMember(x => x.CreateDateUtc, y => y.MapFrom(e => DateTime.UtcNow))
                .ForMember(x => x.Uid, y => y.MapFrom(e => Guid.NewGuid()));

            CreateMap<Order, OrderUpdateDTO>()
                .ReverseMap();

            CreateMap<BrandType, BrandTypeDTO>()
                .ReverseMap();

            CreateMap<SizeType, SizeTypeDTO>()
                .ReverseMap();

            CreateMap<Category, CategoryDTO>()
                .ReverseMap();
        }
    }
}

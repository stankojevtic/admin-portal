﻿using System;
using System.Collections.Generic;
using AdministrationPortal.API.DTO;
using AdministrationPortal.Core.Helpers.Authorization;
using AdministrationPortal.Interfaces.Services;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AdministrationPortal.API.Controllers
{
    [Route("api/brand-types")]
    [ApiController]
    public class BrandTypeController : ControllerBase
    {

        private readonly IBrandTypeService _brandTypeService;
        private readonly IMapper _mapper;

        public BrandTypeController(IBrandTypeService brandTypeService, IMapper mapper)
        {
            _brandTypeService = brandTypeService;
            _mapper = mapper;
        }

        [HttpGet]
        [AuthorizeModel]
        public IActionResult GetAll()
        {
            try
            {
                var brandTypes = _brandTypeService.GetAll();
                var brandTypesDto = _mapper.Map<IEnumerable<BrandTypeDTO>>(brandTypes);

                return Ok(brandTypesDto);
            }
            catch (Exception e)
            {
                //log exception
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
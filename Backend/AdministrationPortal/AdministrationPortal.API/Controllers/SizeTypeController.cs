﻿using System;
using System.Collections.Generic;
using AdministrationPortal.API.DTO;
using AdministrationPortal.Core.Helpers.Authorization;
using AdministrationPortal.Interfaces.Services;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AdministrationPortal.API.Controllers
{
    [Route("api/size-types")]
    [ApiController]
    public class SizeTypeController : ControllerBase
    {

        private readonly ISizeTypeService _sizeTypeService;
        private readonly IMapper _mapper;

        public SizeTypeController(ISizeTypeService sizeTypeService, IMapper mapper)
        {
            _sizeTypeService = sizeTypeService;
            _mapper = mapper;
        }

        [HttpGet]
        [AuthorizeModel]
        public IActionResult GetAll()
        {
            try
            {
                var sizeTypes = _sizeTypeService.GetAll();
                var sizeTypesDto = _mapper.Map<IEnumerable<SizeTypeDTO>>(sizeTypes);

                return Ok(sizeTypesDto);
            }
            catch (Exception e)
            {
                //log exception
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
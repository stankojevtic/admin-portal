﻿using System;
using System.Collections.Generic;
using AdministrationPortal.API.DTO;
using AdministrationPortal.Core.Helpers.Authorization;
using AdministrationPortal.Core.Helpers.Validation;
using AdministrationPortal.Domain.Entities;
using AdministrationPortal.Interfaces.Services;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AdministrationPortal.API.Controllers
{
    [Route("api/products")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _productService;
        private readonly IMapper _mapper;

        public ProductController(IProductService productService, IMapper mapper)
        {
            _productService = productService;
            _mapper = mapper;
        }

        [HttpGet]
        [AuthorizeModel]
        public IActionResult GetAll()
        {
            try
            {
                var products = _productService.GetAll();
                var productsDto = _mapper.Map<IEnumerable<ProductDTO>>(products);

                return Ok(productsDto);
            }
            catch (Exception e)
            {
                //log exception
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet("filter")]
        [AuthorizeModel]
        public IActionResult Filter(string filterText)
        {
            try
            {
                var products = _productService.Filter(filterText);
                var productsDto = _mapper.Map<IEnumerable<ProductDTO>>(products);

                return Ok(productsDto);
            }
            catch (Exception e)
            {
                //log exception
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet("best-buy")]
        [AuthorizeModel]
        public IActionResult BestBuyInLastMonth()
        {
            try
            {
                var products = _productService.GetBestBuyProductsInLastMonth();
                var productsDto = _mapper.Map<IEnumerable<ProductDTO>>(products);
                return Ok(productsDto);
            }
            catch (Exception e)
            {
                //log exception
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet("{id}")]
        [AuthorizeModel]
        public IActionResult Get(int id)
        {
            try
            {
                var product = _productService.GetById(id);
                if (product == null)
                {
                    return NotFound("Product with id " + id + " does not exist.");
                }

                var productDto = _mapper.Map<ProductDTO>(product);

                return Ok(productDto);
            }
            catch (Exception e)
            {
                //log exception
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost]
        [ValidateModel]
        [AuthorizeModel]
        public IActionResult Create([FromBody] ProductCreateDTO productCreateDto)
        {
            try
            {
                var product = _mapper.Map<Product>(productCreateDto);
                _productService.Create(product);
                _productService.AddProductCategories(product.Uid, productCreateDto.Categories);
                _productService.AddSizeTypes(product.Uid, productCreateDto.SizeTypes);

                return Ok();
            }
            catch (Exception e)
            {
                //log exception
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPut("{id}")]
        [ValidateModel]
        [AuthorizeModel]
        public IActionResult Edit(int id, [FromBody] ProductUpdateDTO productUpdateDto)
        {
            try
            {
                var product = _productService.GetById(id);
                if(product == null)
                {
                    return NotFound("Product with id " + id + " does not exist.");
                }
                product = _mapper.Map(productUpdateDto, product);
               
                _productService.Update(product);
                _productService.UpdateCategories(id, productUpdateDto.Categories);
                _productService.UpdateSizeTypes(id, productUpdateDto.SizeTypes);

                return Ok();
            }
            catch (Exception e)
            {
                //log exception
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpDelete("{id}")]
        [AuthorizeModel]
        public IActionResult Delete(int id)
        {
            try
            {
                var product = _productService.GetById(id);
                if(product == null)
                {
                    return NotFound("Product with id " + id + " does not exist.");
                }

                _productService.Delete(product);

                return Ok();
            }
            catch (Exception e)
            {
                //log exception
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using AdministrationPortal.API.DTO;
using AdministrationPortal.Core.Helpers.Authorization;
using AdministrationPortal.Core.Helpers.Validation;
using AdministrationPortal.Domain.Entities;
using AdministrationPortal.Interfaces.Services;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AdministrationPortal.API.Controllers
{
    [Route("api/orders")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService _orderService;
        private readonly IMapper _mapper;

        public OrderController(IOrderService orderService, IMapper mapper)
        {
            _orderService = orderService;
            _mapper = mapper;
        }

        [HttpGet]
        [AuthorizeModel]
        public IActionResult GetAll()
        {
            try
            {
                var orders = _orderService.GetAll();
                var ordersDto = _mapper.Map<IEnumerable<OrderDTO>>(orders);

                return Ok(ordersDto);
            }
            catch (Exception e)
            {
                //log exception
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet("{id}")]
        [AuthorizeModel]
        public IActionResult Get(int id)
        {
            try
            {
                var order = _orderService.GetById(id);
                if (order == null)
                {
                    return NotFound("Order with id " + id + " does not exist.");
                }

                var orderDto = _mapper.Map<OrderDTO>(order);

                return Ok(orderDto);
            }
            catch (Exception e)
            {
                //log exception
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost]
        [ValidateModel]
        [AuthorizeModel]
        public IActionResult Create([FromBody] OrderCreateDTO orderCreateDto)
        {
            try
            {
                var order = _mapper.Map<Order>(orderCreateDto);
                _orderService.Create(order);
                _orderService.AddOrderItems(order.Uid, orderCreateDto.OrderProducts);

                return Ok();
            }
            catch (Exception e)
            {
                //log exception
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPut("{id}")]
        [ValidateModel]
        [AuthorizeModel]
        public IActionResult Edit(int id, [FromBody] OrderUpdateDTO orderUpdateDto)
        {
            try
            {
                var product = _orderService.GetById(id);
                if (product == null)
                {
                    return NotFound("Order with id " + id + " does not exist.");
                }
                product = _mapper.Map(orderUpdateDto, product);

                _orderService.Update(product);
                _orderService.UpdateOrderItems(id, orderUpdateDto.OrderProducts);

                return Ok();
            }
            catch (Exception e)
            {
                //log exception
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpDelete("{id}")]
        [AuthorizeModel]
        public IActionResult Delete(int id)
        {
            try
            {
                var product = _orderService.GetById(id);
                if (product == null)
                {
                    return NotFound("Order with id " + id + " does not exist.");
                }

                _orderService.Delete(product);

                return Ok();
            }
            catch (Exception e)
            {
                //log exception
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
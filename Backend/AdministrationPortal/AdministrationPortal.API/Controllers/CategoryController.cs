﻿using System;
using System.Collections.Generic;
using AdministrationPortal.API.DTO;
using AdministrationPortal.Core.Helpers.Authorization;
using AdministrationPortal.Interfaces.Services;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AdministrationPortal.API.Controllers
{
    [Route("api/categories")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService _categoryService;
        private readonly IMapper _mapper;

        public CategoryController(ICategoryService categoryService, IMapper mapper)
        {
            _categoryService = categoryService;
            _mapper = mapper;
        }

        [HttpGet]
        [AuthorizeModel]
        public IActionResult GetAll()
        {
            try
            {
                var categories = _categoryService.GetAll();
                var categoriesDto = _mapper.Map<IEnumerable<CategoryDTO>>(categories);

                return Ok(categoriesDto);
            }
            catch (Exception e)
            {
                //log exception
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
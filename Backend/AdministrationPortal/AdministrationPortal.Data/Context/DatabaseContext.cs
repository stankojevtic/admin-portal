﻿using System;
using AdministrationPortal.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace AdministrationPortal.Data.Context
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<BrandType> BrandTypes { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<ProductCategory> ProductCategories { get; set; }
        public DbSet<ProductImage> ProductImages { get; set; }
        public DbSet<ProductSizeType> ProductSizeTypes { get; set; }
        public DbSet<SizeType> SizeTypes { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProductCategory>().HasKey(l => new { l.ProductID, l.CategoryID });
            modelBuilder.Entity<ProductSizeType>().HasKey(l => new { l.ProductID, l.SizeTypeID });

            //TODO create separate data seeder class
            modelBuilder.Entity<Category>()
                .HasData(
                    new Category { CategoryID = 1, Caption = "Obuca" },
                    new Category { CategoryID = 2, Caption = "Odeca" },
                    new Category { CategoryID = 3, Caption = "Jakne" },
                    new Category { CategoryID = 4, Caption = "Majice" },
                    new Category { CategoryID = 5, Caption = "Patike" }
                );

            modelBuilder.Entity<BrandType>()
                .HasData(
                    new BrandType { BrandTypeID = 1, Caption = "Nike" },
                    new BrandType { BrandTypeID = 2, Caption = "Adidas" },
                    new BrandType { BrandTypeID = 3, Caption = "Reebok" }
                );

            modelBuilder.Entity<SizeType>()
                .HasData(
                    new SizeType { SizeTypeID = 1, Caption = "S" },
                    new SizeType { SizeTypeID = 2, Caption = "M" },
                    new SizeType { SizeTypeID = 3, Caption = "L" },
                    new SizeType { SizeTypeID = 4, Caption = "XL" },
                    new SizeType { SizeTypeID = 5, Caption = "41" },
                    new SizeType { SizeTypeID = 6, Caption = "42" },
                    new SizeType { SizeTypeID = 7, Caption = "43" },
                    new SizeType { SizeTypeID = 8, Caption = "44" },
                    new SizeType { SizeTypeID = 9, Caption = "45" },
                    new SizeType { SizeTypeID = 10, Caption = "46" }
                );

            //This is not the way to implement it, passwords should never be stored as a plain text. But since
            //there is no registration form in the application I will seed the user this way. In real world application,
            //user password should be encrypted + salted and stored that way.
            modelBuilder.Entity<User>()
                .HasData(
                    new User { UserID = 1, FirstName = "Firstname", LastName = "Lastname", Username = "user1", Password = "user1" }
                );
        }
    }
}
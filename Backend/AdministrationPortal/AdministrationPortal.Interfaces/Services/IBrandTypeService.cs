﻿using AdministrationPortal.Domain.Entities;
using AdministrationPortal.Interfaces.Generic;

namespace AdministrationPortal.Interfaces.Services
{
    public interface IBrandTypeService : IServiceBase<BrandType>
    {
    }
}

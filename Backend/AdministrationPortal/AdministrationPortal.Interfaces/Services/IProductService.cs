﻿using System;
using System.Collections.Generic;
using AdministrationPortal.Domain.Entities;
using AdministrationPortal.Interfaces.Generic;

namespace AdministrationPortal.Interfaces.Services
{
    public interface IProductService : IServiceBase<Product>
    {
        void AddProductCategories(Guid uid, List<object> categories);
        void AddSizeTypes(Guid uid, List<object> sizeTypes);
        void UpdateCategories(int id, List<object> categories);
        void UpdateSizeTypes(int id, List<object> sizeTypes);
        IEnumerable<Product> Filter(string filterText);
        IEnumerable<Product> GetBestBuyProductsInLastMonth();
    }
}

﻿using System;
using System.Collections.Generic;
using AdministrationPortal.Domain.Entities;
using AdministrationPortal.Interfaces.Generic;

namespace AdministrationPortal.Interfaces.Services
{
    public interface IOrderService : IServiceBase<Order>
    {
        void AddOrderItems(Guid uid, List<object> orderProducts);
        void UpdateOrderItems(int id, List<object> orderProducts);
    }
}

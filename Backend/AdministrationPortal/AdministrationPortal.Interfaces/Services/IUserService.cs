﻿using AdministrationPortal.Domain.Entities;
using AdministrationPortal.Interfaces.Generic;

namespace AdministrationPortal.Interfaces.Services
{
    public interface IUserService : IServiceBase<User>
    {
        string Authenticate(string username, string password);
    }
}

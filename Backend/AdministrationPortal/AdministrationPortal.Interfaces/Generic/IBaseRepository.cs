﻿using System.Collections.Generic;
using System.Linq;

namespace AdministrationPortal.Interfaces.Generic
{
    public interface IBaseRepository<T> where T : class
    {
        IQueryable<T> GetQueryable();
        IEnumerable<T> GetAll();
        T Find(int id);
        void Add(T entity, bool saveChanges = true);
        void Edit(T entity, bool saveChanges = true);
        void Delete(T entity, bool saveChanges = true);
        void Save();
        void BulkInsert(IEnumerable<T> entities, bool saveChanges = true);
        void BulkUpdate(IEnumerable<T> entities, bool saveChanges = true);
    }
}

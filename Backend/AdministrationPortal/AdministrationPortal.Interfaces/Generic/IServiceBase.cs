﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace AdministrationPortal.Interfaces.Generic
{
    public interface IServiceBase<TEntity>
    {
        TEntity GetById(int id);
        TEntity GetSingle(Expression<Func<TEntity, bool>> whereCondition);
        void Create(TEntity entity);
        void Delete(TEntity entity);
        void Update(TEntity entity);
        IEnumerable<TEntity> GetAll(Expression<Func<TEntity, bool>> whereCondition);
        IEnumerable<TEntity> GetAll();
        IQueryable<TEntity> Query(Expression<Func<TEntity, bool>> whereCondition);
        long Count(Expression<Func<TEntity, bool>> whereCondition);
        long Count();
        IQueryable<TEntity> GetAllQueryable();
        void BulkInsert(IEnumerable<TEntity> entities);
        void BulkUpdate(IEnumerable<TEntity> entities);
    }
}

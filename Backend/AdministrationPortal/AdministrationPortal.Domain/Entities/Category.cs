﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AdministrationPortal.Domain.Entities
{
    public class Category
    {
        [Key]
        public int CategoryID { get; set; }
        public string Caption { get; set; }
        public virtual ICollection<ProductCategory> ProductCategories { get; set; }
    }
}

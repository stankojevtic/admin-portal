﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AdministrationPortal.Domain.Entities
{
    public class Product
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProductID { get; set; }
        public Guid Uid { get; set; }
        public string Caption { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int BrandTypeID { get; set; }
        public virtual BrandType BrandType { get; set; }
        public virtual ICollection<OrderItem> OrderItems { get; set; }
        public virtual ICollection<ProductImage> ProductImages { get; set; }
        public virtual ICollection<ProductCategory> ProductCategories { get; set; }
        public virtual ICollection<ProductSizeType> ProductSizeTypes { get; set; }
    }
}

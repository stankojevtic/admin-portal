﻿namespace AdministrationPortal.Domain.Entities
{
    public class ProductSizeType
    {
        public int ProductID { get; set; }
        public int SizeTypeID { get; set; }
        public virtual Product Product { get; set; }
        public virtual SizeType SizeType { get; set; }
    }
}

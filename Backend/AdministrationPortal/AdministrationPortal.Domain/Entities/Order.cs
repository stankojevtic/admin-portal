﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AdministrationPortal.Domain.Entities
{
    public class Order
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OrderID { get; set; }
        public Guid Uid { get; set; }
        public decimal TotalPrice { get; set; }
        public DateTime CreateDateUtc { get; set; }
        public virtual ICollection<OrderItem> OrderItems { get; set; }
    }
}

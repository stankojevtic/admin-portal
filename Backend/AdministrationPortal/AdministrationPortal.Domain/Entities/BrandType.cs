﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AdministrationPortal.Domain.Entities
{
    public class BrandType
    {
        [Key]
        public int BrandTypeID { get; set; }
        public string Caption { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}

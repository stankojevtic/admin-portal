﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AdministrationPortal.Domain.Entities
{
    public class SizeType
    {
        [Key]
        public int SizeTypeID { get; set; }
        public string Caption { get; set; }
        public virtual ICollection<ProductSizeType> ProductSizeTypes { get; set; }
    }
}

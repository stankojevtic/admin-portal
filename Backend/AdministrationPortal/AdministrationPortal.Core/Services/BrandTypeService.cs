﻿using AdministrationPortal.Domain.Entities;
using AdministrationPortal.Interfaces.Services;
using AdministrationPortal.Repositories.Repositories;

namespace AdministrationPortal.Core.Services
{
    public class BrandTypeService : ServiceBase<BrandType, BaseRepository<BrandType>>, IBrandTypeService
    {
        public BrandTypeService(BaseRepository<BrandType> rep) : base(rep)
        {
        }
    }
}

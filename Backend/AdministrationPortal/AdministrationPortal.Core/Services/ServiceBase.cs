﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using AdministrationPortal.Interfaces.Generic;
using AdministrationPortal.Repositories.Repositories;

namespace AdministrationPortal.Core.Services
{
    public abstract class ServiceBase<TEntity, TRepository> : IServiceBase<TEntity>
            where TEntity : class
            where TRepository : BaseRepository<TEntity>
    {
        public TRepository Repository;

        public ServiceBase(BaseRepository<TEntity> rep)
        {
            Repository = (TRepository)rep;
        }

        public long Count(Expression<Func<TEntity, bool>> whereCondition)
        {
            return Repository.GetAll().AsQueryable().Where(whereCondition).Count();
        }

        public long Count()
        {
            return Repository.GetAll().Count();
        }

        public virtual void Create(TEntity entity)
        {
            Repository.Add(entity);
        }

        public virtual void Delete(TEntity entity)
        {
            Repository.Delete(entity);
        }

        public IEnumerable<TEntity> GetAll(Expression<Func<TEntity, bool>> whereCondition)
        {
            return Repository.GetAll().AsQueryable().Where(whereCondition);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return Repository.GetAll();
        }
        public IQueryable<TEntity> GetAllQueryable()
        {
            return Repository.GetQueryable();
        }

        public TEntity GetById(int id)
        {
            return Repository.Find(id);
        }

        public TEntity GetSingle(Expression<Func<TEntity, bool>> whereCondition)
        {
            return Repository.GetAll().AsQueryable().Where(whereCondition).FirstOrDefault();
        }

        public IQueryable<TEntity> Query(Expression<Func<TEntity, bool>> whereCondition)
        {
            return Repository.GetAll().AsQueryable().Where(whereCondition);
        }

        public virtual void Update(TEntity entity)
        {
            Repository.Edit(entity);
        }

        public void BulkInsert(IEnumerable<TEntity> entities)
        {
            Repository.BulkInsert(entities);
        }

        public void BulkUpdate(IEnumerable<TEntity> entities)
        {
            Repository.BulkUpdate(entities);
        }
    }
}


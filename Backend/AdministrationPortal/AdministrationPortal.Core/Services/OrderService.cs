﻿using System;
using System.Collections.Generic;
using AdministrationPortal.Domain.Entities;
using AdministrationPortal.Interfaces.Services;
using AdministrationPortal.Repositories.Repositories;
using Newtonsoft.Json.Linq;

namespace AdministrationPortal.Core.Services
{
    public class OrderService : ServiceBase<Order, BaseRepository<Order>>, IOrderService
    {
        private readonly IProductService _productService;
        public OrderService(IProductService productService, BaseRepository<Order> rep) : base(rep)
        {
            _productService = productService;
        }

        public void AddOrderItems(Guid uid, List<object> orderProducts)
        {
            var order = GetSingle(x => x.Uid == uid);
            order.OrderItems= new List<OrderItem>();
            foreach (var orderProduct in orderProducts)
            {
                var productId = (int)JObject.Parse(orderProduct.ToString()).SelectToken("product");
                var product = _productService.GetById(productId);
                order.OrderItems.Add(new OrderItem
                {
                    OrderID = order.OrderID,
                    PricePerUnit = product.Price,
                    ProductID = product.ProductID,
                    Quantity = (int)JObject.Parse(orderProduct.ToString()).SelectToken("quantity")
                });
            }

            Update(order);
        }

        public void UpdateOrderItems(int id, List<object> orderProducts)
        {
            var order = GetById(id);
            order.OrderItems.Clear();
            foreach (var orderProduct in orderProducts)
            {
                var productId = (int)JObject.Parse(orderProduct.ToString()).SelectToken("product");
                var product = _productService.GetById(productId);
                order.OrderItems.Add(new OrderItem
                {
                    OrderID = order.OrderID,
                    PricePerUnit = product.Price,
                    ProductID = product.ProductID,
                    Quantity = (int)JObject.Parse(orderProduct.ToString()).SelectToken("quantity")
                });
            }

            Update(order);
        }
    }
}

﻿using AdministrationPortal.Domain.Entities;
using AdministrationPortal.Interfaces.Services;
using AdministrationPortal.Repositories.Repositories;

namespace AdministrationPortal.Core.Services
{
    public class CategoryService : ServiceBase<Category, BaseRepository<Category>>, ICategoryService
    {
        public CategoryService(BaseRepository<Category> rep) : base(rep)
        {
        }
    }
}

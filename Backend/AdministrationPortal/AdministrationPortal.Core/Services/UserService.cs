﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using AdministrationPortal.Core.Helpers.Authorization;
using AdministrationPortal.Domain.Entities;
using AdministrationPortal.Interfaces.Services;
using AdministrationPortal.Repositories.Repositories;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace AdministrationPortal.Core.Services
{
    public class UserService : ServiceBase<User, BaseRepository<User>>, IUserService
    {
        private readonly AppSettings _appSettings;

        public UserService(BaseRepository<User> rep, IOptions<AppSettings> appSettings) : base(rep)
        {
            _appSettings = appSettings.Value;
        }

        public string Authenticate(string username, string password)
        {
            var user = GetSingle(x => x.Username == username && x.Password == password);

            if (user == null)
            {
                return null;
            }

            return generateJwtToken(user);
        }

        private string generateJwtToken(User user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("id", user.UserID.ToString()) }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}

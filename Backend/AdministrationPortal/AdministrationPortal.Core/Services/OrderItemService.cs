﻿using AdministrationPortal.Domain.Entities;
using AdministrationPortal.Interfaces.Services;
using AdministrationPortal.Repositories.Repositories;

namespace AdministrationPortal.Core.Services
{
    public class OrderItemService : ServiceBase<OrderItem, BaseRepository<OrderItem>>, IOrderItemService
    {
        public OrderItemService(BaseRepository<OrderItem> rep) : base(rep)
        {
        }
    }
}

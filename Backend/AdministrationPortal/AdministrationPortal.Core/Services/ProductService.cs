﻿using System;
using System.Collections.Generic;
using System.Linq;
using AdministrationPortal.Domain.Entities;
using AdministrationPortal.Interfaces.Services;
using AdministrationPortal.Repositories.Repositories;
using Newtonsoft.Json.Linq;

namespace AdministrationPortal.Core.Services
{
    public class ProductService : ServiceBase<Product, BaseRepository<Product>>, IProductService
    {
        private readonly IOrderItemService _orderItemService;
        public ProductService(IOrderItemService orderItemService, BaseRepository<Product> rep) : base(rep)
        {
            _orderItemService = orderItemService;
        }

        public IEnumerable<Product> GetBestBuyProductsInLastMonth()
        {
            var startOfTthisMonth = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
            var firstDay = startOfTthisMonth.AddMonths(-1);
            var lastDay = startOfTthisMonth.AddDays(-1);
            var products = _orderItemService.GetAll()
                                        .GroupBy(x => x.ProductID)
                                        .Select(x => new { ProductId = x.Key, QuantitySum = x.Sum(a => a.Quantity) })
                                        .OrderByDescending(x => x.QuantitySum)
                                        .Select(x => { return GetById(x.ProductId); })
                                        .Take(10).ToList();

            return products;
        }

        public void AddProductCategories(Guid uid, List<object> categories)
        {
            var product = GetSingle(x => x.Uid == uid);
            product.ProductCategories = new List<ProductCategory>();
            foreach (var category in categories)
            {
                product.ProductCategories.Add(new ProductCategory
                {
                    ProductID = product.ProductID,
                    CategoryID = (int)JObject.Parse(category.ToString()).SelectToken("category")
                });
            }

            Update(product);
        }

        public void AddSizeTypes(Guid uid, List<object> sizeTypes)
        {
            var product = GetSingle(x => x.Uid == uid);
            product.ProductSizeTypes = new List<ProductSizeType>();
            foreach (var sizeType in sizeTypes)
            {
                product.ProductSizeTypes.Add(new ProductSizeType
                {
                    ProductID = product.ProductID,
                    SizeTypeID = (int)JObject.Parse(sizeType.ToString()).SelectToken("sizeType")
                });
            }

            Update(product);
        }

        public IEnumerable<Product> Filter(string filterText)
        {
            //optimization - use IQueryable instead of IEnumerable (IQueryable will create query that will execute only once in the database)
            var products = GetAll();

            filterText = filterText.ToLowerInvariant();
            if (!string.IsNullOrEmpty(filterText))
            {
                products = products
                       .Where(x => x.Caption.ToLowerInvariant().Contains(filterText)
                       || x.Description.ToLowerInvariant().Contains(filterText)
                       || x.BrandType.Caption.ToLowerInvariant().Contains(filterText));
            }

            return products;
        }

        public void UpdateCategories(int id, List<object> categories)
        {
            var product = GetById(id);
            product.ProductCategories.Clear();
            foreach (var category in categories)
            {
                product.ProductCategories.Add(new ProductCategory
                {
                    ProductID = product.ProductID,
                    CategoryID = (int)JObject.Parse(category.ToString()).SelectToken("category")
                });
            }

            Update(product);
        }

        public void UpdateSizeTypes(int id, List<object> sizeTypes)
        {
            var product = GetById(id);
            product.ProductSizeTypes.Clear();
            foreach (var sizeType in sizeTypes)
            {
                product.ProductSizeTypes.Add(new ProductSizeType
                {
                    ProductID = product.ProductID,
                    SizeTypeID = (int)JObject.Parse(sizeType.ToString()).SelectToken("sizeType")
                });
            }

            Update(product);
        }
    }
}
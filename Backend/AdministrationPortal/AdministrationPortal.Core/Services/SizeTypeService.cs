﻿using AdministrationPortal.Domain.Entities;
using AdministrationPortal.Interfaces.Services;
using AdministrationPortal.Repositories.Repositories;

namespace AdministrationPortal.Core.Services
{
    public class SizeTypeService : ServiceBase<SizeType, BaseRepository<SizeType>>, ISizeTypeService
    {
        public SizeTypeService(BaseRepository<SizeType> rep) : base(rep)
        {
        }
    }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor() { }

  logOut(): void {
    localStorage.removeItem('jwt');
  }

  isUserAuthenticated(): boolean {
    return localStorage.jwt !== undefined;
  }

  getToken(): string {
    return localStorage.jwt;
  }
}
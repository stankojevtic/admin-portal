import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.scss']
})
export class OrderListComponent implements OnInit {
  ordersHeadElements = ['#', 'Order Items', 'Total price', 'Date created'];
  orderList = null;
  constructor(private httpClient: HttpClient, private router: Router, private toastrService: ToastrService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.refreshOrders();
  }

  refreshOrders() {
    this.httpClient.get('https://localhost:44360/api/orders').subscribe(
      (res) => {
        this.orderList = res;
      },
      (error) => {
        console.log('Could not get orders.');
      }
    )
  }

  onDelete(id: Number) {
    this.httpClient.delete(`https://localhost:44360/api/orders/${id}`).subscribe(
        (res) => {
          this.toastrService.success('Successfully deleted order.', 'Success', { progressBar: true });
          this.refreshOrders();
        },
        (err) => {
          this.toastrService.error('Could not delete order.', 'Error', { progressBar: true });
        }
      )
  }

  onEdit(id: Number) {
    this.router.navigate(['../', 'edit-order', id], {relativeTo: this.route});
  }
}

import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-order',
  templateUrl: './edit-order.component.html',
  styleUrls: ['./edit-order.component.scss']
})
export class EditOrderComponent implements OnInit {
  loaded: boolean = false;
  orderEditForm: FormGroup;
  id: number;
  productList = null;
  orderEdit = null;

  constructor(private httpClient: HttpClient, private route: ActivatedRoute, private router: Router, private toastrService: ToastrService) { }

  ngOnInit() {
    this.route.params.subscribe(
        (params: Params) => {
          this.id = +params['id'];
          this.getData();
          this.initForm();          
        }
    )
  }

  updateTotalPrice() {
    let totalPrice: number;
    totalPrice = 0;
    var orderItems = this.orderEditForm.value.orderProducts;    
    orderItems.forEach(element => {
        var productItem = this.productList.find(x => x.productID == element.product)
        if(productItem){     
            totalPrice = totalPrice + (productItem.price * element.quantity)
        }
    });  

    this.orderEditForm.patchValue({
        totalPrice: totalPrice
    })
  }

  getData() {
    this.httpClient.get('https://localhost:44360/api/products').subscribe(
          (res) => {
            this.productList = res;
          }
    )
  }

  onSubmit()
  {
    this.httpClient.put(`https://localhost:44360/api/orders/${this.id}`, this.orderEditForm.value).subscribe(
        (res) => {
          this.toastrService.success('Successfully updated order.', 'Success', { progressBar: true });
          this.router.navigate(['orders']);
        },
        (error) => {
          console.log(error)
          this.toastrService.error('Could not update order.', 'Error', { progressBar: true });
          this.router.navigate(['orders']);
        }
    )
  }

  initForm() {
    this.httpClient.get(`https://localhost:44360/api/orders/${this.id}`).subscribe(
        (res) => {
          this.orderEdit = res;
          let orderProducts = new FormArray([]);

          this.orderEdit.orderItems.forEach(y => {              
            orderProducts.push(new FormGroup({
                'product': new FormControl(y.productID, Validators.required),
                'quantity': new FormControl(y.quantity, Validators.required)
              })
            );
          })   
          
          this.orderEditForm = new FormGroup({
              'orderProducts': orderProducts,
              'totalPrice': new FormControl(this.orderEdit.totalPrice)
          })
          this.orderEditForm.get('orderProducts').valueChanges.subscribe(changes => {
            setTimeout(() => {
                this.updateTotalPrice();
            }, 1000)
          })
          this.loaded = true;
        }
    )
  }

  onAddOrderProduct() {
    (<FormArray>this.orderEditForm.get('orderProducts')).push(
      new FormGroup({
        'product': new FormControl(null, Validators.required),
        'quantity': new FormControl(1, Validators.required)
      })
    );
  }

  onDeleteOrderProduct(index: number) {
    (<FormArray>this.orderEditForm.get('orderProducts')).removeAt(index);
  }
}

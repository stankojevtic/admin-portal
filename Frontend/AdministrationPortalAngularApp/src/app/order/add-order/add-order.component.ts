import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-order',
  templateUrl: './add-order.component.html',
  styleUrls: ['./add-order.component.scss']
})
export class AddOrderComponent implements OnInit {
  orderCreateForm: FormGroup;
  productList = null;
  constructor(private httpClient: HttpClient, private router: Router, private toastrService: ToastrService) { }

  ngOnInit() {
    this.getData();
    this.initForm();
  }

  updateTotalPrice() {
    let totalPrice: number;
    totalPrice = 0;
    var orderItems = this.orderCreateForm.value.orderProducts;    
    orderItems.forEach(element => {
        var productItem = this.productList.find(x => x.productID == element.product)
        if(productItem){     
            totalPrice = totalPrice + (productItem.price * element.quantity)
        }
    });  

    this.orderCreateForm.patchValue({
        totalPrice: totalPrice
    })
  }

  getData() {
    this.httpClient.get('https://localhost:44360/api/products').subscribe(
          (res) => {
            this.productList = res;
          }
    )
  }

  onSubmit()
  {
    this.httpClient.post('https://localhost:44360/api/orders', this.orderCreateForm.value).subscribe(
        (res) => {
          this.toastrService.success('Successfully created product.', 'Success', { progressBar: true });
          this.router.navigate(['orders']);
        },
        (error) => {
          this.toastrService.error('Could not create product.', 'Error', { progressBar: true });
          this.router.navigate(['orders']);
        }
    )
  }

  initForm() {
    let orderProducts = new FormArray([]);
    orderProducts.push(new FormGroup({
        'product': new FormControl(null, Validators.required),
        'quantity': new FormControl(1, Validators.required)
      })
    );

    this.orderCreateForm = new FormGroup({
        'orderProducts': orderProducts,
        'totalPrice': new FormControl(0)
    })
    this.orderCreateForm.get('orderProducts').valueChanges.subscribe(changes => {
        setTimeout(() => {
            this.updateTotalPrice();
        }, 1000)
    })
  }

  onAddOrderProduct() {
    (<FormArray>this.orderCreateForm.get('orderProducts')).push(
      new FormGroup({
        'product': new FormControl(null, Validators.required),
        'quantity': new FormControl(1, Validators.required)
      })
    );
  }

  onDeleteOrderProduct(index: number) {
    (<FormArray>this.orderCreateForm.get('orderProducts')).removeAt(index);
  }
}

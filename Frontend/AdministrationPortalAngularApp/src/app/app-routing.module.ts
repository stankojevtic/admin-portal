import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { ProductListComponent } from './product/product-list/product-list.component';
import { AddProductComponent } from './product/add-product/add-product.component';
import { EditProductComponent } from './product/edit-product/edit-product.component';
import { OrderListComponent } from './order/order-list/order-list.component';
import { AddOrderComponent } from './order/add-order/add-order.component';
import { EditOrderComponent } from './order/edit-order/edit-order.component';

const appRoutes: Routes = [
    { path: '', redirectTo:'home', pathMatch:'full'},
    { path: 'home', component: HomeComponent },
    { path: 'login', component: LoginComponent},
    { path: 'products', component: ProductListComponent},
    { path: 'create-product', component: AddProductComponent},
    { path: 'edit-product/:id', component: EditProductComponent },
    { path: 'orders', component: OrderListComponent},
    { path: 'create-order', component: AddOrderComponent},
    { path: 'edit-order/:id', component: EditOrderComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})
export class AppRoutingModule {

}
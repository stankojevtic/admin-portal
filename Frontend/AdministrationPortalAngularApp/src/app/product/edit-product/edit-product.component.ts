import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.scss']
})
export class EditProductComponent implements OnInit {
  loaded: boolean = false;
  productEditForm: FormGroup;
  id: number;
  brandTypeList = null;
  categoryList = null;
  sizeTypeList = null;
  productEdit = null;
  constructor(private httpClient: HttpClient, private route: ActivatedRoute, private router: Router, private toastrService: ToastrService) { }

  ngOnInit() {
    this.route.params.subscribe(
        (params: Params) => {
          this.id = +params['id'];
          this.getData();
          this.initForm();
        }
      )
  }

  getData() {
    //Optimize this to be retreived from 1 endpoint
    this.httpClient.get('https://localhost:44360/api/brand-types').subscribe(
        (res) => {
          this.brandTypeList = res;
        }
    )

    this.httpClient.get('https://localhost:44360/api/categories').subscribe(
        (res) => {
          this.categoryList = res;
        }
    )

    this.httpClient.get('https://localhost:44360/api/size-types').subscribe(
        (res) => {
          this.sizeTypeList = res;
        }
    )
  }

  onSubmit()
  {
      if(this.hasDuplicates(this.productEditForm.value.categories, this.productEditForm.value.sizeTypes)) {
        this.toastrService.error('Categories or Sizes cant have duplicates.', 'Error', { progressBar: true });
      } else if (!this.priceIsPositiveValueLargerThanZero(this.productEditForm.value.price)) {
        this.toastrService.error('Price must be larger than 0.', 'Error', { progressBar: true });
      } else {
        this.httpClient.put(`https://localhost:44360/api/products/${this.id}`, this.productEditForm.value).subscribe(
            (res) => {
              this.toastrService.success('Successfully edited product.', 'Success', { progressBar: true });
              this.router.navigate(['products']);
            },
            (error) => {
              this.toastrService.error('Could not edit product.', 'Error', { progressBar: true });
            }
        )
      }
  }

  onAddCategory() {
    (<FormArray>this.productEditForm.get('categories')).push(
      new FormGroup({
        'category': new FormControl(null, Validators.required)
      })
    );
  }

  onDeleteCategory(index: number) {
    (<FormArray>this.productEditForm.get('categories')).removeAt(index);
  }

  onAddSizeType() {
    (<FormArray>this.productEditForm.get('sizeTypes')).push(
      new FormGroup({
        'sizeType': new FormControl(null, Validators.required)
      })
    );
  }

  onDeleteSizeType(index: number) {
    (<FormArray>this.productEditForm.get('sizeTypes')).removeAt(index);
  }

  initForm() {
    this.httpClient.get(`https://localhost:44360/api/products/${this.id}`).subscribe(
        (res) => {
          this.productEdit = res;
               
          let categories = new FormArray([]);
          this.productEdit.categories.forEach(y => {              
            categories.push(new FormGroup({
                'category': new FormControl(this.categoryList.find(x => x.caption == y).categoryID.toString(), Validators.required)
              })
            );
          })    
        
          let sizeTypes = new FormArray([]);
          this.productEdit.sizeTypes.forEach(y => {              
            sizeTypes.push(new FormGroup({
                'sizeType': new FormControl(this.sizeTypeList.find(x => x.caption == y).sizeTypeID.toString(), Validators.required)
              })
            );
          })         
        
          this.productEditForm = new FormGroup({
            'caption': new FormControl(this.productEdit.caption, Validators.required),
            'description': new FormControl(this.productEdit.description, Validators.required),
            'price': new FormControl(this.productEdit.price, Validators.required),
            'brandType': new FormControl(this.brandTypeList.find(x => x.caption == this.productEdit.brandType).brandTypeID, Validators.required),
            'categories': categories,
            'sizeTypes': sizeTypes
          })
          this.loaded = true;
        }
    )
  }

  hasDuplicates(categories, sizeTypes) {
    var categoriesValueArr = categories.map(function(item){ return item.category });
    var sizeTypesValueArr = sizeTypes.map(function(item){ return item.sizeType });
    var categoriesHasDuplicate = categoriesValueArr.some(function(item, idx){ 
        return categoriesValueArr.indexOf(item) != idx 
    });
    var sizeTypesHasDuplicate = sizeTypesValueArr.some(function(item, idx){ 
        return sizeTypesValueArr.indexOf(item) != idx 
    });
    return (categoriesHasDuplicate || sizeTypesHasDuplicate);
  }

  priceIsPositiveValueLargerThanZero(price) {
      return price > 0;
  }

}

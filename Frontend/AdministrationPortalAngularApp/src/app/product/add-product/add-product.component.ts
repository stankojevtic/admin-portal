import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {
  productCreateForm: FormGroup;
  brandTypeList = null;
  categoryList = null;
  sizeTypeList = null;

  constructor(private httpClient: HttpClient, private router: Router, private toastrService: ToastrService) { }

  ngOnInit() {
      this.getData();
      this.initForm();
  }  

  getData() {
    //Optimize this to be retreived from 1 endpoint
    this.httpClient.get('https://localhost:44360/api/brand-types').subscribe(
        (res) => {
          this.brandTypeList = res;
        }
    )

    this.httpClient.get('https://localhost:44360/api/categories').subscribe(
        (res) => {
          this.categoryList = res;
        }
    )

    this.httpClient.get('https://localhost:44360/api/size-types').subscribe(
        (res) => {
          this.sizeTypeList = res;
        }
    )
  }

  onAddCategory() {
    (<FormArray>this.productCreateForm.get('categories')).push(
      new FormGroup({
        'category': new FormControl(null, Validators.required)
      })
    );
  }

  onDeleteCategory(index: number) {
    (<FormArray>this.productCreateForm.get('categories')).removeAt(index);
  }

  onAddSizeType() {
    (<FormArray>this.productCreateForm.get('sizeTypes')).push(
      new FormGroup({
        'sizeType': new FormControl(null, Validators.required)
      })
    );
  }

  onDeleteSizeType(index: number) {
    (<FormArray>this.productCreateForm.get('sizeTypes')).removeAt(index);
  }

  onSubmit()
  {
      if(this.hasDuplicates(this.productCreateForm.value.categories, this.productCreateForm.value.sizeTypes)) {
        this.toastrService.error('Categories or Sizes cant have duplicates.', 'Error', { progressBar: true });
      } else if (!this.priceIsPositiveValueLargerThanZero(this.productCreateForm.value.price)) {
        this.toastrService.error('Price must be larger than 0.', 'Error', { progressBar: true });
      } else {
        this.httpClient.post('https://localhost:44360/api/products', this.productCreateForm.value).subscribe(
            (res) => {
              this.toastrService.success('Successfully created product.', 'Success', { progressBar: true });
              this.router.navigate(['products']);
            },
            (error) => {
              this.toastrService.error('Could not create product.', 'Error', { progressBar: true });
              this.router.navigate(['products']);
            }
        )
      }
  }

  initForm() {
    let categories = new FormArray([]);
    let sizeTypes = new FormArray([]);
    categories.push(new FormGroup({
        'category': new FormControl(null, Validators.required)
      })
    );
    for(let i=0; i<3; i++){
        sizeTypes.push(new FormGroup({
            'sizeType': new FormControl(null, Validators.required)
          })
        );
    }

    this.productCreateForm = new FormGroup({
      'caption': new FormControl(null, Validators.required),
      'description': new FormControl(null, Validators.required),
      'price': new FormControl(0, Validators.required),
      'brandType': new FormControl(null, Validators.required),
      'categories': categories,
      'sizeTypes': sizeTypes
    })
  }

  hasDuplicates(categories, sizeTypes) {
    var categoriesValueArr = categories.map(function(item){ return item.category });
    var sizeTypesValueArr = sizeTypes.map(function(item){ return item.sizeType });
    var categoriesHasDuplicate = categoriesValueArr.some(function(item, idx){ 
        return categoriesValueArr.indexOf(item) != idx 
    });
    var sizeTypesHasDuplicate = sizeTypesValueArr.some(function(item, idx){ 
        return sizeTypesValueArr.indexOf(item) != idx 
    });
    return (categoriesHasDuplicate || sizeTypesHasDuplicate);
  }

  priceIsPositiveValueLargerThanZero(price) {
    return price > 0;
  }
}

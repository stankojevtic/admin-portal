import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
  productList = null;
  productsHeadElements = ['#', 'Caption', 'Description', 'Price', 'Brand', 'Categories', 'Sizes'];
  constructor(private httpClient: HttpClient, private router: Router, private toastrService: ToastrService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.refreshProducts();
  }

  refreshProducts() {
    this.httpClient.get('https://localhost:44360/api/products').subscribe(
      (res) => {
        this.productList = res;
      },
      (error) => {
        console.log('Could not get products.');
      }
    )
  }

  onDelete(id: Number) {
    this.httpClient.delete(`https://localhost:44360/api/products/${id}`).subscribe(
        (res) => {
          this.toastrService.success('Successfully deleted product.', 'Success', { progressBar: true });
          this.refreshProducts();
        },
        (err) => {
          this.toastrService.error('Could not delete product.', 'Error', { progressBar: true });
        }
      )
  }

  onEdit(id: Number) {
    this.router.navigate(['../', 'edit-product', id], {relativeTo: this.route});
  }
}

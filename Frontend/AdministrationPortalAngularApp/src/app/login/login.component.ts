import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    
    constructor(private httpClient: HttpClient, private router: Router, private toastrService: ToastrService) 
    { 
    }
  
    ngOnInit() {
      this.loginForm = new FormGroup({
        'username': new FormControl(null, Validators.required),
        'password': new FormControl(null, Validators.required)
      })
    }
  
    onSubmit(){
      this.httpClient.post('https://localhost:44360/api/users/login', this.loginForm.value, { responseType: 'text' }).subscribe(
        (res) => {
          localStorage.setItem('jwt', res.toString());
          this.toastrService.success('Successfully logged in.', 'Success', { progressBar: true });
          this.router.navigate(['home']);
        },
        (error) => {
          this.toastrService.error('Could not log in. Username or password might be incorrect.', 'Error', { progressBar: true });
        }
      )    
    }
  }
  